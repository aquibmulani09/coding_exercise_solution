#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <string.h>


static char const *input_filename[2];                   /* File names of the files to be compared  */
static long file1s;
static long file2s;
static char logPath[50];
static FILE *test_file[2];                              /* File Description of the files to be compared  */
static FILE *logs;
static int error_size = 16;
static int errors = 0;

/*Get size of the files*/
static long getSize(FILE *fp) 
{
    fseek(fp, 0, SEEK_END);
    long size = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    return size;
}

void ComputeErrorString(int offset, char *errorString1, char *errorString2) 
{
    rewind(test_file[0]);
    rewind(test_file[1]);
    if (fseek(test_file[0], offset, SEEK_SET) == 0) 
    {
       int last =  fread(errorString1, sizeof(unsigned char), error_size, test_file[0]);
       errorString1[last]='\0';

    }
    if (fseek(test_file[1], offset, SEEK_SET) == 0) 
    {
        int last = fread(errorString2, sizeof(unsigned char), error_size, test_file[1]);
        errorString1[last]='\0';
    }
}

/*This function compares 2 files and returns the error position in the file*/
int mycompare(char *data1, char *data2, const long bytesToFetch) 
   {
    int file1ok = 0;
    int file2ok = 0;
    int offset;
    file1ok = fread(data1, sizeof(unsigned char), bytesToFetch, test_file[0]);
    file2ok = fread(data2, sizeof(unsigned char), bytesToFetch, test_file[1]);
    if (file1ok && file2ok) 
    {
        for (int i = 0; i < bytesToFetch; i++)
            if ((data1[i] != data2[i])) 
            {
                errors++;                                                                           //save the offset
                offset = i;
                break;
            }
        return offset;
    } 
    else 
    {
        return 0;
    }
}


int main(int argc, char **argv) 
{

    input_filename[0] = argv[1];
    input_filename[1] = argv[2];

    test_file[0] = fopen(input_filename[0], "r");                                 //read file 1
    test_file[1] = fopen(input_filename[1], "r");                                 //read file 2

    file1s = getSize(test_file[0]);                                                 
    file2s = getSize(test_file[1]);

    getcwd(logPath,50);                                                           //get path of the current working directory
    strcat(logPath, "/logs.txt");
    logs = fopen(logPath, "w");


    /*if any of the paths are invalid; return error */
    if (test_file[0] == NULL || test_file[1] == NULL)
    {
        fprintf(logs, "One of the files is empty");
    }
    if (logs == NULL) 
    {
        perror("Invalid file or Directory");
    }
    if (file1s != file2s)
    {                                                       
        fprintf(logs, "Error! Files are of different size");
        return 0;
    }
    if (file1s ==0 && file2s == 0) 
    {                                                  
        fprintf(logs, "Error ! Files are empty");
        return 0;
    } 
    else 
    {                                                                                  
        clock_t begin = clock();                                              //start time
        unsigned char dataBuffer_file1[file1s];
        unsigned char dataBuffer_file2[file2s];
        int offset = 0;
        char errorStringF1[17]={};
        char errorStringF2[17] = {};
        long bytesToFetch = file1s;
        fprintf(logs, "Job Started at %lfs\n", (double) begin / CLOCKS_PER_SEC);
   		int newoff=0;
   		
   		newoff = mycompare(dataBuffer_file1, dataBuffer_file2, bytesToFetch); 
        if (errors != 0) 
        {
            fprintf(logs, "Error found at offset= %d\n", newoff);
            if ((file1s - offset) < error_size)                                      
            {
                error_size = file1s - offset;
                fprintf(logs, "Error String size Updated to %d\n", error_size);
            }

            ComputeErrorString(newoff, errorStringF1,errorStringF2);                   //compute the error strings to be printed
            fprintf(logs, "String from first file= %s\n", errorStringF1);
            fprintf(logs, "String from second file= %s\n", errorStringF2);
        } 
        else 
        {
            fprintf(logs, "Files are identical\n");
        }

        clock_t end = clock();                                                   //end time
        fprintf(logs, "Job Ended at %lf seconds\n", (double) end / CLOCKS_PER_SEC);
        double time_spent = (double) (end - begin) / CLOCKS_PER_SEC;
        fprintf(logs, "Total Time taken= %lf seconds\n", time_spent);

    }

    fclose(test_file[1]);
    fclose(test_file[0]);
    fclose(logs);

    return 0;
}