#include <stdio.h>

/*
 * Given a byte array(data) of length(len) search for the starting position
 * of 32 bit word 0xDEADBEEF and return the position
 * If it is not found return -1
*/
int search_sync(unsigned char *data, int len)
{
    int j=0;
    int i=0;
    unsigned char word_arr[]={0xde,0xad,0xbe,0xef};
    
        while(i<len)
        {
        if(data[i]==0xde)
        {
            if(((i<len) && data[i+1]==0xad) && ((i+2)<len && data[i+2]==0xbe) && ((i+3)<len && data[i+3]==0xef) )
            {
                j=i;
                return j;
            }
        }
        i++;
    }

    return -1;
}
unsigned char d1[] = {   0x12,   0x34,  0x56,   0x78, 0xde, 0xad, 0xbe, 0xef, 0, 0 };
unsigned char d2[] = {   0x12,   0x34,  0x56,   0x78, 0xde, 0xad, 0xbe, 0xe0, 0, 0 }; 

int main(void)
{
    int pos = -1;
    printf("In Main1\n");

    pos = search_sync(d1,10);
    printf("pos=%d\n",pos);
    pos = search_sync(d1,7);
    printf("pos=%d\n",pos);
    pos = search_sync(d2,10);
    printf("pos=%d\n",pos);

    return 0;
}