#include <stdio.h>
#include <stdint.h>

uint8_t get_byte(uint8_t *ptr,int length,int bit_position)
{
	if(length==0)
	{		
		return 0;
	}
	int bit_length = 8 * length;
	if((bit_position+8)>bit_length)
	{
			
		return 0;
	}
	else
	{
   uint8_t current_position=0,current_byte=0;
   uint16_t combine_var=0;
   uint8_t extract_varaible=0;
    current_position= bit_position%8;          
	current_byte= bit_position/8;	           
	combine_var |= (ptr[current_byte+1]);
	combine_var = (combine_var<<8) | ((ptr[current_byte]));
	combine_var>>=current_position;
    extract_varaible = combine_var & 0xFF;
	return extract_varaible;
	}
}


int main()
{
 uint8_t arr[]={0xff,0x0f,4};
 
 for(int i=0;i<16;i++)
 {
 	printf("i=%d %x\n",i,get_byte(arr,3,i));	
 }

 return 0;
}